$(document).ready(function(){
    
    // Initiating functions when loading the page.
	$(window).on('load', function(){
        showModal("introductory_rules"); // Pops-up introductory_rules  modal
        // Initiates transition effect of bubbles in background by calling function from js-file.js
        childCSSmaker("bubble", 100, "bg-bubbles");
        $("#opening-bg").css("display", "none");
    });


	
	// Validating Modal form...
	// It checks all the checkboxes of introductory_rules, whether checked or not.
	// If all the check boxes are checked, it will enable submit button, and,
	// if anyone remains unchecked, submit button will always be disabled.
	$('input[name="rule"]').click(function () {
		if(($("#rule1").prop('checked') == true) && ($("#rule2").prop('checked') == true) && ($("#rule3").prop('checked') == true)){
			$("#submitModal").prop('disabled',false);
	    }
	    else{
	    	$("#submitModal").prop('disabled',true);
	    }
	});



	// Generating marquee with the help of jQuery, calling and updating marquee
	// in-built function. ".marquee" here is the class of element which is converted into marquee.
	$('.marquee').marquee({
		// speed in milliseconds of the marquee
		duration: 4000,
		// gap in pixels between the tickers
		gap: 0,
		// time in milliseconds before the marquee will start animating
		delayBeforeStart: 0,
		// direction of the marquee. Value: left, right, up, down
		direction: 'up',
		// should the marquee be duplicated, will marquee repeat or not. Value: true or false
		duplicated: true,
		// should marquee will pause after completing one cycle or not. Value: true or false
		pauseOnCycle: true,
		// should marquee will pause on mouse hover or not. Value: true or false
		pauseOnHover: true,
	});

	

	// // Used to stop outside scrolling of the page, when current element is scrollable.
	// $('.section-divide').on('mousewheel DOMMouseScroll', function (e) { 
 //    	var e0 = e.originalEvent;
 //    	var delta = e0.wheelDelta || -e0.detail;
 //    	this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
 //    	e.preventDefault();  
 //  	});

	// // Used to stop outside scrolling of the page, when current element is scrollable.
	// $('.section-divide-inner').on('mousewheel DOMMouseScroll', function (e) { 
 //    	var e0 = e.originalEvent;
 //    	var delta = e0.wheelDelta || -e0.detail;
 //    	this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
 //    	e.preventDefault();  
 //  	});


	// Used to show the tooltip on the element. Here it is used for image
	// in section 3 part 1 on image 1.
	$('[data-toggle="tooltip"]').tooltip({animation: true});


	// Validating game form in section 4 part 1.
	// It calls the function triggerSuccessFormEntry() from js-file.js
	// whenever user clicks on textarea of the game form.
	$("textarea[name='gameForm']").click(function () {
		triggerSuccessFormEntry("gameForm");
	});


});
