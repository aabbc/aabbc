// ============ Global Variables Declarations ============

var modalBodyContent = ['a', 'b', 'c', 'd'];//, 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                        // 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                        // 'w', 'x', 'y', 'z'];
var modalTrackVar = 0;








// Function used to display modal. Param: Id of the modal to display.
function showModal(ids) {
  // Backdrop static prevents the modal to close when clicking outside the modal, 
  // or by pressing any key from the keyboard.
  $("#" + ids).modal({backdrop: "static"}); 
  $("#" + ids).on("shown.bs.modal", function(){
      $('.modal-backdrop.in').css('opacity', '1');
      // $('.modal-backdrop').css('background-color', 'yellow');
      $('.modal-backdrop').css('background', "url('https://wallpapercave.com/wp/wp3284839.gif')");
      $('.modal-backdrop').css('height', '100%');
      $('.modal-backdrop').css('background-position', 'center');
      $('.modal-backdrop').css('background-repeat', 'no-repeat');
      $('.modal-backdrop').css('background-size', "cover");
  });
}


// Funtion used for the repetitive pop-up modal.
// This function shows modal at every 2 minutes from the closing of 
// the previous shown modal.
function repeatModal() {
  // Using two global variables : modalTrackVar, modalBodyContent
  if(modalTrackVar == modalBodyContent.length){
    return;
  }
  document.getElementById("fixed-time-modal-body").innerHTML = modalBodyContent[modalTrackVar];
  modalTrackVar++;

  var progress = Math.round((modalTrackVar / modalBodyContent.length) * 100);
  document.getElementById("popUpModalProgress").innerHTML = modalTrackVar + "/" + modalBodyContent.length;

  $('#popUpModalProgress').attr('aria-valuenow', progress).css('width', progress + '%');
  
  setTimeout(function () {
    $('#popUpModal').modal({backdrop: "static"});
    }, 1000 * 60 * 2); // 2 minutes = (1000 milli seconds * 60 seconds *  2)
}


// Function used to add/ modify CSS property and also adding new tags.
// This function is responsible for creating bubbles in background.
// Params: class of element, no. of elements to show (iteration no.), id of element 
function childCSSmaker(ele_class, n, ids) {
  for(i = 1; i < n; i++){
    $('#' + ids).append('<div class="' + ele_class + '"></div>');
  }
  
  var sheet = document.styleSheets[0];
  for(i = 1; i < n; i++){

    var wh = Math.floor(Math.random() * 50) + 10;
    var l = Math.floor(Math.random() * 80) + 10;
    var ad = Math.floor(Math.random() * 14) + 5;
    var ade = Math.floor(Math.random() * 2) + 1; 
    var rule = '.' + ele_class + ':nth-child(' + i + '){ width: ' + wh +'px; height: ' + wh + 'px; left: ' + l + '%; animation-duration: ' + ad + 's; animation-delay: ' + ade + 's;}';
    sheet.insertRule(rule, 1);
  }
}



// Function used to regulate navigation buttons (forward/ backward).
// It update the links, direction and control when to enable/disable,
// display/undisplay the navigation buttons.
// Params: element tag, id of the element.
function navNext(ele, id) {
  // if we have reached here, then one of the button has definitely been clicked.

  var nextID = "navNextButton";
  var preID = "navPreviousButton";
  var imgDown = "<img class=\"nav-arrow-img\" src=\"static/down-arrow.png\" alt=\"Arrow Down\">";
  var imgUp = "<img class=\"nav-arrow-img\" src=\"static/up-arrow.png\" alt=\"Arrow Up\">";
  var imgRight = "<img class=\"nav-arrow-img\" src=\"static/right-arrow.png\" alt=\"Arrow Right\">";
  var imgLeft = "<img class=\"nav-arrow-img\" src=\"static/left-arrow.png\" alt=\"Arrow Left\">";

  var arr = ['#section1-part1', '#section1-part2', '#section2-part1', '#section2-part2',
  '#section3-part1', '#section3-part2', '#section4-part1', '#section4-part2', '#section5-part1',
  
   '#section5-part2'];

  var currentLink = ele.getAttribute("data-current-link");
  var direction = ele.getAttribute('data-direction');
  var z = arr.indexOf(currentLink);

  // if forward button is clicked.
  if(id == nextID){
    ele.setAttribute('data-current-link', arr[z+1]);
    document.getElementById(preID).setAttribute('data-current-link', currentLink);
    document.getElementById(preID).removeAttribute('disabled');
    // document.getElementById(document.getElementById(preID).getAttribute('data-parent-id')).style.display = "block";
    document.getElementById(document.getElementById(preID).getAttribute('data-parent-id')).style.visibility = "visible";

    // check if it wasn't the last section of the page.
    if(ele.getAttribute('data-current-link') == arr[arr.length - 1]){
      // disable the forward button.
      ele.setAttribute("disabled", "true");
      // document.getElementById(document.getElementById(id).getAttribute('data-parent-id')).style.display = "none";
      document.getElementById(document.getElementById(id).getAttribute('data-parent-id')).style.visibility = "hidden";
    }
    window.location = ele.getAttribute('data-current-link');
  }

  // if backward button is clicked.
  else if(id == preID){
    ele.setAttribute('data-current-link', arr[z-1]);
    document.getElementById(nextID).setAttribute('data-current-link', currentLink);
    document.getElementById(nextID).removeAttribute('disabled');
    // document.getElementById(document.getElementById(nextID).getAttribute('data-parent-id')).style.display = "block";
    document.getElementById(document.getElementById(nextID).getAttribute('data-parent-id')).style.visibility = "visible";

    // check if it wasn't the first section of the page.
    if(ele.getAttribute('data-current-link') == "undefined"){
      // disable the backward button.
      ele.setAttribute("disabled", "true");
      // document.getElementById(document.getElementById(id).getAttribute('data-parent-id')).style.display = "none";
      document.getElementById(document.getElementById(id).getAttribute('data-parent-id')).style.visibility = "hidden";
    }
    window.location = currentLink;
  }  

  // Updating direction & animation of forward button.
  if(!document.getElementById(nextID).disabled){
    var nextLink = document.getElementById(nextID).getAttribute('data-current-link');
    // Setting animation and direction.
    var direction = (nextLink.charAt(nextLink.length - 1) == '1') ? 'right' : 'down';
    document.getElementById(document.getElementById(nextID).getAttribute('data-parent-id')).style.animation = "2s ease-in infinite jump-" + direction;
    // Rotating element to fix the arrow.
    document.getElementById(nextID).innerHTML = (direction == 'right') ? imgRight : imgDown;
  }

  // Updating direction and animation of backward button.
  if(!document.getElementById(preID).disabled){
    var nextLink = document.getElementById(preID).getAttribute('data-current-link');
    // Setting animation and direction.
    var direction = (nextLink.charAt(nextLink.length - 1) == '1') ? 'right' : 'down';
    document.getElementById(document.getElementById(preID).getAttribute('data-parent-id')).style.animation = "2s ease-in infinite jump-" + direction;
    // Rotating element to fix the arrow.
    document.getElementById(preID).innerHTML = (direction == 'right') ? imgLeft : imgUp;
  }
}



// Function used to display image larger at middle of the page,
// and to reduce the image back to its original size.
// Function gets triggered when clicked on the image.
// Params: element tag, flag (0 or 1 only),
//         0 -> Enlargement of the image.
//         1 -> Move image back to original size.
function enlarge1(ele, flag) {
  if(flag == 0){
    // This is called for enlargement, i.e. "onmouseenter".
    var source = ele.getAttribute("src");
    document.getElementById("collage-pop").setAttribute("src", source);
    document.getElementById("collage-pop").style.display = "block";
  }
  else if(flag == 1){
    // This is called for shrinkment, i.e. "onmouseleave".
    document.getElementById("collage-pop").style.display = "none";
  }
}

function enlarge(ele, flag) {
  if(flag == 0){
    // This is called for enlargement, i.e. "onmouseenter/ zoom".
    if(ele.getAttribute("data-element-type") == "image"){
      var source = ele.getAttribute("src");
      var element = "<img src=" + source + " \"alt=\"Image Pop-up\" class=\"pop-up-image\">";
    }
    else{
      var source = ele.getAttribute("data-source");
      var dataType = ele.getAttribute("data-type");
      var element = "<video controls loop class=\"pop-up-vedio\">" + 
      "<source src=" + source + " type=" + dataType + "></video>";

    }
    document.getElementById("collage-pop").innerHTML = element;
    document.getElementById("collage-pop").style.display = "block";
  }
  else if(flag == 1){
    // This is called for shrinkment, i.e. "onmouseleave/ zoom-out".
    document.getElementById("collage-pop").style.display = "none";
  }
}

// Function used to show the success of the textbox area.
// It marks the indicator as green if text is present in textbox.
// Function gets triggered when textarea is clicked with ele_name
// Params: name of the element tag
function triggerSuccessFormEntry(ele_name) {
  var x = document.getElementsByName(ele_name);
  for(var i = 0; i < x.length; i++){
    if((x[i].value.trim()) != ""){
      x[i].style.border = "4px solid #5cb85c";
      x[i].nextElementSibling.style.display = "block";
    }
    else{
      x[i].style.removeProperty("border");
      x[i].nextElementSibling.style.display = "none";
    }
  }
}



function createDownloadLink(id, str, fileName){
  if(window.navigator.msSaveOrOpenBlob) {
    var fileData = [str];
    var blobObject = new Blob(fileData);
    $(id).click(function(){
      window.navigator.msSaveOrOpenBlob(blobObject, fileName);
    });
  } else {
    var url = "data:text/plain;charset=utf-8," + encodeURIComponent(str);
    $(id).attr("href", url);
  }
}

